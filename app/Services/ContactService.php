<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName($name = ''): Contact
	{
		// queries to the db
		$contacts = array(
			0 => array("name" => "contact1", "number" => "111111111"),
			1 => array("name" => "contact2", "number" => "222222222"),
			2 => array("name" => "contact3", "number" => "333333333"),
			3 => array("name" => "contact4", "number" => "444444444"),
			4 => array("name" => "contact5", "number" => "555555555"),
		);
		$contact_key = array_search($name, array_column($contacts, 'name'));
		
		// Validate that a contact has been found 
		if( $contact_key !== false )
		{
			$contact = $contacts[$contact_key];
			return new Contact($contact['name'], $contact['number']);
		}

		return new Contact();
	}

	public static function findByNumber($number = ''): Contact
	{
		// queries to the db
		$contacts = array(
			0 => array("name" => "contact1", "number" => "111111111"),
			1 => array("name" => "contact2", "number" => "222222222"),
			2 => array("name" => "contact3", "number" => "333333333"),
			3 => array("name" => "contact4", "number" => "444444444"),
			4 => array("name" => "contact5", "number" => "555555555"),
		);
		$contact_key = array_search($number, array_column($contacts, 'number'));

		// Validate that a contact has been found 
		if( $contact_key !== false )
		{
			$contact = $contacts[$contact_key];
			return new Contact($contact['name'], $contact['number']);
		}

		return new Contact();
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
		// only 9 digit numbers are accepted
		if(preg_match("/^[0-9]{9}$/", $number))
		{
			return true;
		}

		return false;
	}
}
