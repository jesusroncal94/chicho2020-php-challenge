<?php

namespace App;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;


class Provider implements CarrierInterface
{

    function __construct()
	{
		# code...
	}

	public function dialContact(Contact $contact)
	{
		#code
	}

    public function makeCall(): Call
    {
        $call = new Call();
        return $call;
    }
}
