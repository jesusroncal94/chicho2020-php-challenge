<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);
		return $contact->number;

		// $this->provider->dialContact($contact);

		// return $this->provider->makeCall();
	}

	public function sendMessage($number = array("number" => '', "message" => ''))
	{
		if( empty($number) ) return;

		$number_is_valid = ContactService::validateNumber($number["number"]);
		if ( $number_is_valid === false ) return;

		$contact = ContactService::findByNumber($number["number"]);
		if( empty($contact->name) ) return;
		
		return 'Message sent';
	}
}
