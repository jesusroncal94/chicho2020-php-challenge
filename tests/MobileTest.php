<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Provider;
use App\Services\ContactService;


class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function pass_valid_contact_to_make_call_by_name()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertEquals('111111111', $mobile->makeCallByName('contact1'));
	}

	/** @test */
	public function check_if_contact_is_found_by_name()
	{
		$name = 'contact1';
		$contact = ContactService::findByName($name);

		$this->assertEquals('111111111', $contact->number);
	}

	/** @test */
	public function check_if_contact_is_not_found_by_name()
	{
		$name = 'contactNotFound';
		$contact = ContactService::findByName($name);

		$this->assertEquals('', $contact->number);
	}

	/** @test */
	public function send_message_with_valid_number()
	{
		$body = array(
			"number" => "111111111",
			"message" => "Hi, bro!"
		);

		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertEquals('Message sent', $mobile->sendMessage($body));
	}

	/** @test */
	public function send_message_with_invalid_number()
	{
		$body = array(
			"number" => "xxxxxxxxx",
			"message" => "Hi, bro!"
		);

		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->sendMessage($body));
	}

	/** @test */
	public function check_if_contact_is_found_by_number()
	{
		$number = '111111111';
		$contact = ContactService::findByNumber($number);

		$this->assertEquals('contact1', $contact->name);
	}

	/** @test */
	public function check_if_contact_is_not_found_by_number()
	{
		$number = '000000000';
		$contact = ContactService::findByNumber($number);

		$this->assertEquals('', $contact->name);
	}

	/** @test */
	/** @test */
	public function check_if_number_is_valid()
	{
		$number = '111111111';

		$this->assertEquals(true, ContactService::validateNumber($number));
	}

	/** @test */
	public function check_if_number_is_invalid()
	{
		$number = '1111x1111';

		$this->assertEquals(false, ContactService::validateNumber($number));
	}
}
